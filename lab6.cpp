/*
Isaiah Griner
CPSC 1021, 3, F20
igriner@clemson.edu
Nushrat
*/


#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include <cstdlib>

using namespace std;

typedef struct Employee{
  string lastName;
  string firstName;
  int birthYear;
  double hourlyWage;
}employee;
bool name_order(const employee& lhs, const employee& rhs);
int myrandom (int i) {
   return rand()%i;
}
int main(int argc, char const *argv[]) {
    // IMPLEMENT as instructed below
    /*This is to seed the raNew Empty Filendom generator */
    srand(unsigned (time(0)));
    /*Create an array of 10 employees and fill information from standard input with prompt messages*/
    employee array[10];
    for(int i=0; i<10; ++i){
      std::cout <<"Please enter Employee #" << i+1 <<"'s last name."<<endl;
      std::cin >> array[i].lastName;

      std::cout <<"Please enter Employee #" << i+1 <<"'s first name."<<endl;
      std::cin >> array[i].firstName;

      std::cout <<"Please enter Employee #" << i+1 <<"'s birth year."<<endl;
      std::cin >> array[i].birthYear;

      std::cout <<"Please enter Employee #" << i+1 <<"'s hourly wage."<<endl;
      std::cin >> array[i].hourlyWage;
    }
    /*After the array is created and initialzed we call random_shuffle() see the   *notes to determine the parameters to pass in.*/
    employee *end;
    end = &array[10];

    employee *start;
    start = &array[0];

    random_shuffle(start, end, myrandom);
    /*Build a smaller array of 5 employees from the first five cards of the array created    *above*/
    employee sarray[5];
    for(int i=0; i <5; ++i){
      sarray[i].lastName = array[i].lastName;
      sarray[i].firstName = array[i].firstName;
      sarray[i].birthYear = array[i].birthYear;
      sarray[i].hourlyWage = array[i].hourlyWage;
    }
    /*Sort the new array.  Links to how to call this function is in the specs     *provided*/
    employee *send;
    send = &sarray[5];

    employee *sstart;
    sstart = &sarray[0];
    sort(sstart, send, name_order);
    /*Now print the array below */
    for(int i=0; i <5; ++i){
      std::cout <<setw(20)<<right<<sarray[i].lastName<<","<<sarray[i].firstName<<endl;
      std::cout <<setw(20)<<right<<sarray[i].birthYear<<endl;
      std::cout <<fixed<<setprecision(2)<<setw(20)<<right<<sarray[i].hourlyWage<<endl;
    }
  return 0;
}
/*This function will be passed to the sort funtion. Hints on how to implement* this is in the specifications document.*/
bool name_order(const employee& lhs, const employee& rhs) {
  if(lhs.lastName < rhs.lastName)return true;
  else return false;
}
